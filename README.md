Too much password crypto library
================================

[![CircleCI branch](https://img.shields.io/circleci/project/too-much-password/too-much-password-crypto/master.svg?maxAge=2592000)](https://circleci.com/gh/too-much-password/too-much-password-crypto)
[![Github All Releases](https://img.shields.io/github/downloads/too-much-password/too-much-password-crypto/total.svg?maxAge=2592000)]()
[![Code Climate](https://codeclimate.com/github/too-much-password/too-much-password-crypto/badges/gpa.svg)](https://codeclimate.com/github/too-much-password/too-much-password-crypto)

Tests
-----

- Use [expect.js](https://github.com/Automattic/expect.js) as assertion framework
- Use [mocha](https://github.com/mochajs/mocha) as runner
