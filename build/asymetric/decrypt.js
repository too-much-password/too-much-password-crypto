'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _openpgp = require('openpgp');

var decryption = function decryption(privateKey, encrypted) {
  return (0, _openpgp.decrypt)({
    message: _openpgp.message.readArmored(encrypted),
    privateKey: _openpgp.key.readArmored(privateKey).keys[0]
  }).then(function (decrypted) {
    return decrypted.data;
  });
};

exports.default = decryption;