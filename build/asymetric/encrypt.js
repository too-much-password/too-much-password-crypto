'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _openpgp = require('openpgp');

var encryption = function encryption(publicKey, message) {
  return (0, _openpgp.encrypt)({
    data: message,
    publicKeys: _openpgp.key.readArmored(publicKey).keys,
    armor: true
  }).then(function (encrypted) {
    return encrypted.data;
  });
};

exports.default = encryption;