'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _openpgp = require('openpgp');

var generate = function generate(user) {
  var size = arguments.length <= 1 || arguments[1] === undefined ? 4096 : arguments[1];
  var passphrase = arguments.length <= 2 || arguments[2] === undefined ? null : arguments[2];

  var options = {
    userIds: Array.isArray(user) ? user : [user],
    numBits: size,
    passphrase: passphrase
  };
  return (0, _openpgp.generateKey)(options).then(function (keyPair) {
    return {
      publicKey: keyPair.publicKeyArmored,
      privateKey: keyPair.privateKeyArmored
    };
  });
};

exports.default = generate;