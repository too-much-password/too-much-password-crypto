'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.generate = exports.checkValidity = exports.generateCharset = exports.SYMBOLS = exports.NUMBERS = exports.LETTERS = undefined;

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _openpgp = require('openpgp');

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var LETTERS = exports.LETTERS = 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN'.split('');
var NUMBERS = exports.NUMBERS = '1234567890'.split('');
var SYMBOLS = exports.SYMBOLS = '#{[|\\^@]}=)_-("\'&$*%!:;,?./<>'.split('');

var generateCharset = exports.generateCharset = function generateCharset(options) {
  var charset = [];
  if (options.letters) charset.push.apply(charset, _toConsumableArray(LETTERS));
  if (options.numbers) charset.push.apply(charset, _toConsumableArray(NUMBERS));
  if (options.symbols) charset.push.apply(charset, _toConsumableArray(SYMBOLS));
  return charset;
};

var checkValidity = exports.checkValidity = function checkValidity(options, value) {
  if (options.letters && (0, _lodash.intersection)(value.split(''), LETTERS).length == 0) return false;
  if (options.numbers && (0, _lodash.intersection)(value.split(''), NUMBERS).length == 0) return false;
  if (options.symbols && (0, _lodash.intersection)(value.split(''), SYMBOLS).length == 0) return false;
  return true;
};

var generate = exports.generate = function generate(options) {
  var charset = generateCharset(options);
  var result = '';
  do {
    result = '';
    for (var i = 0; i < options.length; i++) {
      result += charset[_openpgp.crypto.random.getSecureRandom(0, charset.length - 1)];
    }
  } while (!checkValidity(options, result));
  return _bluebird2.default.resolve(result);
};

exports.default = generate;