'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.randoms = undefined;

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _lodash = require('lodash');

var _sjcl = require('sjcl');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var randoms = exports.randoms = {};

exports.default = function () {
  var paranoia = arguments.length <= 0 || arguments[0] === undefined ? 8 : arguments[0];
  var autoEntropy = arguments[1];

  if (!randoms[paranoia]) {
    randoms[paranoia] = new _bluebird2.default(function (resolve, reject) {
      var rand = new _sjcl.prng(paranoia);
      var ready = setInterval(function () {
        if (autoEntropy) {
          var buf = new Uint32Array(32);
          window.crypto.getRandomValues(buf);
          rand.addEntropy(buf, 1024, 'crypto.randomBytes');
        }
        if (rand.isReady(paranoia) > 0) {
          rand.stopCollectors();
          clearInterval(ready);
          resolve(rand);
        }
      }, 500);
      rand.startCollectors();
    });
  }
  return randoms[paranoia];
};