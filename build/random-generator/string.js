'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _lodash = require('lodash');

var _sjcl = require('sjcl');

var _randomizer = require('./randomizer');

var _randomizer2 = _interopRequireDefault(_randomizer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var generate = function generate(length, paranoia, autoEntropy) {
  return (0, _randomizer2.default)(paranoia, autoEntropy).then(function (rand) {
    return rand.randomWords(length, paranoia);
  }).then(function (result) {
    return [].concat(_toConsumableArray(result)).map(function (i) {
      return Math.abs(i).toString(36);
    }).join('');
  }).then(function (result) {
    return result.substring(0, length);
  });
};

exports.default = generate;