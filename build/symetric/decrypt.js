'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _openpgp = require('openpgp');

var decryption = function decryption(password, encrypted) {
  return (0, _openpgp.decrypt)({
    message: _openpgp.message.readArmored(encrypted),
    password: password
  }).then(function (decrypted) {
    return decrypted.data;
  });
};

exports.default = decryption;