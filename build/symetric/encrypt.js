'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _openpgp = require('openpgp');

var encryption = function encryption(password, message) {
  return (0, _openpgp.encrypt)({
    data: message,
    passwords: Array.isArray(password) ? password : [password]
  }).then(function (encrypted) {
    return encrypted.data;
  });
};

exports.default = encryption;