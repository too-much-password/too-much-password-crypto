import {decrypt, key, message} from 'openpgp';

const decryption = (privateKey, encrypted) => {
  return decrypt({
    message: message.readArmored(encrypted),
    privateKey: key.readArmored(privateKey).keys[0],
  }).then(decrypted => decrypted.data);
};

export default decryption;
