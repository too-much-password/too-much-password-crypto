import expect from 'expect.js';
import generator from './keygen.js';
import encrypt from './encrypt.js';
import decrypt from './decrypt.js';

describe('asymetric/decrypt', function() {
  this.message = 'Hello world !';

  beforeEach(done => {
    generator({name: 'Chuck Norris', email: 'chuck@norris.com'}, 1024)
      .then((keyPair) => {
        this.keyPair = keyPair
      }).then(done).catch(done);
  });

  beforeEach(done => {
    encrypt(this.keyPair.publicKey, this.message)
      .then(encrypted => {
        this.encrypted = encrypted;
      }).then(done).catch(done);
  });

  it('should return an decryption function', () => {
    expect(decrypt).to.be.an('function');
  });

  it('should decrypt the message', (done) => {
    decrypt(this.keyPair.privateKey, this.encrypted)
      .then(decrypted => {
        expect(decrypted).to.be.a('string');
        expect(decrypted).to.eql(this.message);
      }).then(done).catch(done);
  });

});
