import {encrypt, key} from 'openpgp';

const encryption = (publicKey, message) => {
  return encrypt({
    data: message,
    publicKeys: key.readArmored(publicKey).keys,
    armor: true,
  }).then(encrypted => encrypted.data);
};

export default encryption;
