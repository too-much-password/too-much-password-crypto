import expect from 'expect.js';
import generator from './keygen.js';
import encrypt from './encrypt.js';

describe('asymetric/encrypt', function() {
  this.timeout(1000 * 60 * 5); // generating key pair is quite long

  beforeEach((done) => {
    generator({name: 'Chuck Norris', email: 'chuck@norris.com'}, 1024)
      .then((keyPair) => {
        this.keyPair = keyPair
      }).then(done);
  });

  it('should return an encryption function', () => {
    expect(encrypt).to.be.an('function');
  });

  it('should encrypt the message', (done) => {
    encrypt(this.keyPair.publicKey, 'Hello !')
      .then(encrypted => {
        expect(encrypted).to.be.a('string');
      }).then(done);
  });

});
