import expect from 'expect.js';
import * as asymetric from './index.js';

describe('asymetric/index', function() {

  it('should load', () => {
    expect(asymetric).to.be.ok();
    expect(asymetric.decrypt).to.be.ok();
    expect(asymetric.encrypt).to.be.ok();
    expect(asymetric.keygen).to.be.ok();
  });

});
