import {generateKey} from 'openpgp';

const generate = (user, size = 4096, passphrase = null) => {
  let options = {
    userIds: Array.isArray(user) ? user : [user],
    numBits: size,
    passphrase
  };
  return generateKey(options)
    .then((keyPair) => {
      return {
        publicKey: keyPair.publicKeyArmored,
        privateKey: keyPair.privateKeyArmored
      }
    });
}

export default generate;
