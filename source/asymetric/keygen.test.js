import expect from 'expect.js';
import generator from './keygen.js';

const checkKeyPair = (key) => {
  expect(key).to.be.an('object');
  expect(key).to.have.property('privateKey');
  expect(key.privateKey).to.be.a('string');
  expect(key).to.have.property('publicKey');
  expect(key.publicKey).to.be.a('string');
};

describe('asymetric/keygen', function() {
  this.timeout(1000 * 60 * 5); // generating key pair is quite long

  it('should return a generator', () => {
    expect(generator).to.be.a('function');
  });

  [1024, 2048].forEach((size) => {

    it(`should return a key pair of size ${size} with a passphrase`, (done) => {
      generator({name: 'Chuck Norris', email: 'chuck@norris.com'}, size, 'this is a passphrase')
        .then(checkKeyPair)
        .then(done)
        .catch(done);
    });

    it(`should return a key pair of size ${size} without passphrase`, (done) => {
      generator({name: 'Chuck Norris', email: 'chuck@norris.com'}, size)
        .then(checkKeyPair)
        .then(done)
        .catch(done);
    });

  });

});
