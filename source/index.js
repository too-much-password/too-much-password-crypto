import * as asymetric from './asymetric';
import * as symetric from './symetric';
import * as passwordGenerator from './password-generator';
import * as randomGenerator from './random-generator';

export {asymetric, passwordGenerator, randomGenerator, symetric};
export default {asymetric, passwordGenerator, randomGenerator, symetric};
