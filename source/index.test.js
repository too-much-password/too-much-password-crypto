import expect from 'expect.js';
import crypto from './index.js';

describe('index', function() {

  it('should load', () => {
    expect(crypto).to.be.ok();
    expect(crypto.asymetric).to.be.ok();
    expect(crypto.symetric).to.be.ok();
    expect(crypto.passwordGenerator).to.be.ok();
    expect(crypto.passwordGenerator.generate).to.be.a('function');
    expect(crypto.randomGenerator).to.be.ok();
  });

});
