import Promise from 'bluebird';
import {crypto} from 'openpgp';
import {intersection} from 'lodash';

export const LETTERS = 'azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN'.split('');
export const NUMBERS = '1234567890'.split('');
export const SYMBOLS = '#{[|\\^@]}=)_-("\'&$*%!:;,?./<>'.split('');

export const generateCharset = options => {
  let charset = [];
  if (options.letters) charset.push(...LETTERS);
  if (options.numbers) charset.push(...NUMBERS);
  if (options.symbols) charset.push(...SYMBOLS);
  return charset;
};

export const checkValidity = (options, value) => {
  if (options.letters && intersection(value.split(''), LETTERS).length == 0) return false;
  if (options.numbers && intersection(value.split(''), NUMBERS).length == 0) return false;
  if (options.symbols && intersection(value.split(''), SYMBOLS).length == 0) return false;
  return true;
};

export const generate = options => {
  let charset = generateCharset(options);
  let result = '';
  do {
    result = '';
    for (let i = 0; i < options.length; i++) {
      result += charset[crypto.random.getSecureRandom(0, charset.length-1)];
    }
  } while (!checkValidity(options, result));
  return Promise.resolve(result);
};

export default generate;
