import expect from 'expect.js';
import jsdom from 'jsdom-global';
import generator from './index.js';

const verify = config => {
  return res => {
    expect(res).to.exist;
    expect(res.length).to.eql(config.length);
  };
}

describe('password-generator/index', function() {
  // this.timeout(1000 * 60);

  before(() => this.jsdom = jsdom());
  before(() => {
    window.crypto = {
      getRandomValues: function(array) {
        for (let i = 0; i<array.length; i++) {
          array[i] = Math.random() * 100;
        }
      }
    }
  });
  after(() => this.jsdom());

  it('should return a generator', () => {
    expect(generator).to.be.a('function');
  });

  [
    {letters: true, numbers: true, symbols: true, length: 8},
    {letters: true, numbers: true, symbols: true, length: 16},
    {letters: true, numbers: true, symbols: false, length: 16},
  ].map(config => {
    it(`should generate a password fitting ${JSON.stringify(config)}`, done => {
      generator(config)
        .then(verify(config))
        .then(done)
        .catch(done);
    });
  });

});
