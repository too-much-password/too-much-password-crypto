import expect from 'expect.js';
import {randomizer, string} from './index';

describe('random-generator/index', function() {

  it('should export randomizer', () => {
    expect(randomizer).to.be.a('function');
  });

  it('should export string', () => {
    expect(string).to.be.a('function');
  });

});
