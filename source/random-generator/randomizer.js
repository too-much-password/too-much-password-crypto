import Promise from 'bluebird';
import {map} from 'lodash';
import {prng} from 'sjcl';

export const randoms = {};

export default (paranoia = 8, autoEntropy) => {
  if (!randoms[paranoia]) {
    randoms[paranoia] = new Promise((resolve, reject) => {
      const rand = new prng(paranoia);
      let ready = setInterval(() => {
        if (autoEntropy) {
          let buf = new Uint32Array(32);
          window.crypto.getRandomValues(buf);
          rand.addEntropy(buf, 1024, 'crypto.randomBytes');
        }
        if (rand.isReady(paranoia) > 0) {
          rand.stopCollectors();
          clearInterval(ready);
          resolve(rand);
        }
      }, 500);
      rand.startCollectors();
    });
  }
  return randoms[paranoia];
};

