import expect from 'expect.js';
import jsdom from 'jsdom-global';
import getRandom from './randomizer';

describe('random-generator/randomizer', function() {
  this.timeout(1000 * 60);

  before(() => this.jsdom = jsdom());
  before(() => {
    window.crypto = {
      getRandomValues: function() {
      }
    }
  });
  after(() => this.jsdom());

  it('should define a function', () => {
    expect(getRandom).to.be.a('function');
  });

});
