import Promise from 'bluebird';
import {map} from 'lodash';
import {prng} from 'sjcl';
import getRandom from './randomizer';

const generate = (length, paranoia, autoEntropy) => {
  return getRandom(paranoia, autoEntropy)
    .then(rand => rand.randomWords(length, paranoia))
    .then(result => [...result].map(i => Math.abs(i).toString(36)).join(''))
    .then(result => result.substring(0, length));
};

export default generate;
