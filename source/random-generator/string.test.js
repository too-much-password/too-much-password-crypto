import expect from 'expect.js';
import jsdom from 'jsdom-global';
import sinon from 'sinon';
import generate from './string';

describe('random-generator/string', function() {
  this.timeout(1000 * 60);

  before(() => this.jsdom = jsdom());
  before(() => {
    window.crypto = {
      getRandomValues: function() {}
    }
  });
  after(() => this.jsdom());

  it('should define a function', () => {
    expect(generate).to.be.a('function');
  });

  // length
  [8, 16, 32, 64, 128, 256, 512, 1024, 2048, 4096].forEach((length) => {

    // paranoia
    [2,4,6,8,10].forEach(paranoia => {
      it(`should return a string of size ${length} with paranoia at ${paranoia}`, (done) => {
        generate(length, paranoia, true).then((result) => {
          expect(result).to.be.a('string');
          expect(result).to.have.length(length);
        }).then(done).catch(done);
      });
    });
  });

});
