import {decrypt, key, message} from 'openpgp';

const decryption = (password, encrypted) => {
  return decrypt({
    message: message.readArmored(encrypted),
    password,
  }).then(decrypted => decrypted.data);
};

export default decryption;
