import expect from 'expect.js';
import encrypt from './encrypt.js';
import decrypt from './decrypt.js';

describe('asymetric/decrypt', function() {
  this.password = 'This is an awesome password';
  this.message = 'Hello world !';

  before(done => {
    encrypt(this.password, this.message)
      .then(encrypted => {
        this.encrypted = encrypted;
      }).then(done).catch(done);
  });

  it('should return an decryption function', () => {
    expect(decrypt).to.be.an('function');
  });

  it('should decrypt the message', (done) => {
    decrypt(this.password, this.encrypted)
      .then(decrypted => {
        expect(decrypted).to.be.a('string');
        expect(decrypted).to.eql(this.message);
      }).then(done).catch(done);
  });

});
