import {encrypt, key} from 'openpgp';

const encryption = (password, message) => {
  return encrypt({
    data: message,
    passwords: Array.isArray(password) ? password : [password]
  }).then(encrypted => encrypted.data);
};

export default encryption;
