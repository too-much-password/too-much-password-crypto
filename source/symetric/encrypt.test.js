import expect from 'expect.js';
import encrypt from './encrypt.js';

describe('symetric/encrypt', function() {

  it('should return an encryption function', () => {
    expect(encrypt).to.be.an('function');
  });

  it('should encrypt the message', (done) => {
    encrypt('An awesome password', 'Hello !')
      .then(encrypted => {
        expect(encrypted).to.be.a('string');
      }).then(done);
  });

});
