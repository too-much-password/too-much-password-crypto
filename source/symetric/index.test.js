import expect from 'expect.js';
import * as symetric from './index.js';

describe('symetric/index', function() {

  it('should load', () => {
    expect(symetric).to.be.ok();
    expect(symetric.decrypt).to.be.ok();
    expect(symetric.encrypt).to.be.ok();
  });

});
